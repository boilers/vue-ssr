const fs = require('fs')
const Vue = require('vue')
const server = require('express')()

const { createRenderer } = require('vue-server-renderer')
const renderer = createRenderer({
  template: fs.readFileSync('./public/index.template.html', 'utf-8')
})

const context = {
  title: 'vue-ssr',
  meta: `
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
  `
}

server.get('*', async (req, res) => {
  const app = new Vue({
    data: {
      url: req.url
    },
    template: `<div>Request url is {{ url }}</div>`
  })

  try {
    const html = await renderer.renderToString(app, context)
    res.end(html)
  } catch (err) {
    console.log(err)
    res.status(500).end('Internal server error.')
  }
})

server.listen(8080)
